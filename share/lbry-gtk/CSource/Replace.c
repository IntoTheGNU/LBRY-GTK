/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for switching the current view (list, publications)
// and for getting what the current view is

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <stdarg.h>
#include <stdbool.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <ctype.h>

#include "Header/Replace.h"

ReplaceData *ReplaceInit(GtkWidget **GotWidgets, char **GotNames,
	GtkWidget *GotSpace, int GotLength) {
	// This function is responsible for creating main data structure containing
	// Everything needed by functions

	// Create data struct and arrays in it
	ReplaceData *Data = malloc(sizeof(ReplaceData));
	Data->Widgets = malloc(sizeof(GtkWidget *) * GotLength);
	Data->Names = malloc(sizeof(char *) * GotLength);

	// Copy GotWidgets into Widgets
	memcpy(Data->Widgets, GotWidgets, sizeof(GtkWidget *) * GotLength);

	int MaxNameLength = 0;
	for (int Index = 0; Index < GotLength; ++Index) {
		// Get length of current name, calculate max length from it
		int NameLength = strlen(GotNames[Index]) + 1;
		MaxNameLength = Max(MaxNameLength, NameLength);

		// Allocate memory for string, copy name into it
		Data->Names[Index] = malloc(NameLength);
		sprintf(Data->Names[Index], "%s", GotNames[Index]);
	}

	// Set Space, and number of items in arrays
	Data->Space = GotSpace;
	Data->Length = GotLength;

	// Allocate memory for keeping current widget in Space, and fiil it with " "
	Data->Current = malloc(MaxNameLength);
	sprintf(Data->Current, " ");

	// Return struct with every data needed for operation
	return Data;
}

gboolean ReplaceRemove(GtkWidget *Widget, gpointer Space) {
	// This function is responsible for removing Widget from Space

	// Remove Widget from Space
	gtk_container_remove((GtkContainer *) Space, Widget);

	return FALSE;
}

void ReplaceSet(ReplaceData *Data, char *Name) {
	// This function is responsible for replacing current widget with one called
	// by Name

	// If current widget is requested, do nothing
	if (strcmp(Data->Current, Name) == 0) {
		return;
	}

	// Set current widget to requested, empty Space
	sprintf(Data->Current, "%s", Name);
	gtk_container_foreach((GtkContainer *) Data->Space,
		(GtkCallback) ReplaceRemove, Data->Space);

	for (int Index = 0; Index < Data->Length; ++Index) {
		// If Name is found
		if (strcmp(Data->Names[Index], Name) == 0) {
			// Get widget and its parent
			GtkWidget *Widget = Data->Widgets[Index];
			GtkWidget *Parent = gtk_widget_get_parent(Widget);

			// If it has a parent, remove from it
			if (Parent != NULL) {
				gtk_container_remove((GtkContainer *) Parent, Widget);
			}

			// Add it to Space
			gtk_container_add((GtkContainer *) Data->Space, Widget);
			break;
		}
	}
}

char *ReplaceGet(ReplaceData *Data) {
	// This function is responsible for getting the name of the current widget

	// Get current widget in Space
	return Data->Current;
}

void ReplaceFree(ReplaceData *Data) {
	// This function is responsible for freeing up memory allocated by Init

	// Free up every alloced memory
	free(Data->Widgets);
	for (int Index = 0; Index < Data->Length; ++Index) {
		free(Data->Names[Index]);
	}

	free(Data->Names);
	free(Data);
}

// Everything under this line is removable after full C conversion
static PyObject *ReplaceInitPython(PyObject *Self, PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill Object1, Object2 and LongPointer with sent data
	PyObject *Object1, *Object2;
	long LongPointer;
	if (!PyArg_ParseTuple(Args, "OOl", &Object1, &Object2, &LongPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Get length of python list, create arrays of same length
	int ListLength = PyObject_Length(Object1);
	GtkWidget *WidgetList[ListLength];
	char *NameList[ListLength];

	// Get python objects per index, convert and put them into c arrays
	for (int Index = 0; Index < ListLength; ++Index) {
		PyObject *Widget, *Name;
		Widget = PyList_GetItem(Object1, Index);
		Name = PyList_GetItem(Object2, Index);
		WidgetList[Index] = (GtkWidget *) PyLong_AsLong(Widget);
		NameList[Index] = (char *) PyUnicode_AsUTF8(Name);
	}

	// Get Space
	GtkWidget *Space = (GtkWidget *) LongPointer;

	// Pass arguments to the pythonless function
	ReplaceData *Data;
	Data = ReplaceInit(WidgetList, (char **) NameList, Space, ListLength);

	// Acquire lock
	PyEval_RestoreThread(Save);

	// Return what it returns
	return PyLong_FromVoidPtr(Data);
}

static PyObject *ReplaceSetPython(PyObject *Self, PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill LongPointer, Name with sent data
	long LongPointer;
	char *Name;
	if (!PyArg_ParseTuple(Args, "ls", &LongPointer, &Name)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass arguments to the pythonless function
	ReplaceSet((ReplaceData *) LongPointer, Name);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *ReplaceGetPython(PyObject *Self, PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill LongPointer with sent data
	long LongPointer;
	if (!PyArg_ParseTuple(Args, "l", &LongPointer)) {
		return NULL;
	}

	// Pass arguments to the pythonless function, return what it returns
	PyObject *Return = PyUnicode_FromString(ReplaceGet((ReplaceData *)
			LongPointer));

	return Return;
}

static PyObject *ReplaceFreePython(PyObject *Self, PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill LongPointer with sent data
	long LongPointer;
	if (!PyArg_ParseTuple(Args, "l", &LongPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass arguments to the pythonless function
	ReplaceFree((ReplaceData *) LongPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

// Function names and other python data
static PyMethodDef ReplaceMethods[] = {
	{"Init", ReplaceInitPython, METH_VARARGS, "Init"},
	{"Replace", ReplaceSetPython, METH_VARARGS, "Replace"},
	{"GetSpace", ReplaceGetPython, METH_VARARGS, "Get Space"},
	{"Free", ReplaceFreePython, METH_VARARGS, "Free"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ReplaceModule = {
	PyModuleDef_HEAD_INIT, "Replace", "Replace module", -1, ReplaceMethods
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Replace(void) {
	return PyModule_Create(&ReplaceModule);
}
