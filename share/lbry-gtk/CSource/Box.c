/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for managing the Box widget

#include <gtk/gtk.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Header/Global.h"
#include "Header/Places.h"
#include "Header/GTKExtra.h"
#include "Header/Column.h"
#include "Header/Profile.h"
#include "Header/Image.h"
#include "Header/Box.h"

void BoxReparent(gpointer Widget, GtkWidget *RowColumn) {
	// This function is responsible for unpareting a widget and adding it to
	// RowColumn

	// Get parent, remove from it, add to RowColumn
	GtkWidget *Parent = gtk_widget_get_parent((GtkWidget *) Widget);
	gtk_container_remove((GtkContainer *) Parent, (GtkWidget *) Widget);
	gtk_container_add((GtkContainer *) RowColumn, (GtkWidget *) Widget);
}

BoxData *BoxCreate(bool Grid, int Width, BoxPublicationData *Row,
	PyObject *GetPublicationThread, int TitleRows, int Padding,
	int Type, PyObject *AddPage, bool ThumbnailRounding,
	bool ProfileCircling, PyObject *Lister, int ListChannelWidth,
	GtkWidget *FlowBox, bool PublicationProfile,
	int PublicationProfileSize) {
	// This function is responsible for creating the Box widget

	// Allocate memory for data, abort if NULL
	BoxData *Data = malloc(sizeof(BoxData));
	if (Data == NULL) {
		abort();
	}

	// Calculate max width, for channel name in profile channel button
	int MaxWidth = (Grid) ? Width + 2 * Padding : ListChannelWidth;

	// Create profile
	Data->Profiler = ProfileCreate(Row->Creator, Row->Profile,
			Row->Value, Row->Support, Row->Number, Row->TimeStamp,
			PublicationProfileSize, PublicationProfile, GetPublicationThread,
			AddPage, Lister, Row->Name, ProfileCircling, MaxWidth);

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 15];
	sprintf(WidgetFile, "%sBox.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Get widgets used
	Data->Type = (GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Type");
	Data->Box = (GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Box");
	Data->Thumbnail =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Thumbnail");

	// Set got/default values
	Data->TextWidth = Width;
	Data->Grid = Grid;
	Data->GetPublicationThread = GetPublicationThread;
	Data->AddPage = AddPage;
	Data->TitleRows = TitleRows;
	Data->Url = NULL;
	Data->Title = NULL;

	// Get url, if given and possible
	if (Row->Url != NULL) {
		Data->Url = malloc(strlen(Row->Url) + 1);
		if (Data->Url != NULL) {
			sprintf(Data->Url, "%s", Row->Url);
		}
	}

	// Get title of post if given and possible
	if (Row->Title != NULL) {
		Data->Title = malloc(strlen(Row->Title) + 1);
		if (Data->Title != NULL) {
			sprintf(Data->Title, "%s", Row->Title);
		}
	}

	// Set thumbnail size
	gtk_widget_set_size_request(Data->Thumbnail, Width, Width / 16 * 9);

	// Add profile to widget
	GObject *ProfileBox = gtk_builder_get_object(GlobalBuilder, "ProfileBox");
	gtk_container_add((GtkContainer *) ProfileBox, Data->Profiler->Profile);

	// Check if publication is a channel and if it needs rounding or circling
	bool Channel = strcmp(Row->Type, "channel") == 0;
	bool Mode = (Channel && ProfileCircling) || (!Channel && ThumbnailRounding);

	// Set thumbnail
	ImageUrl(Row->Thumbnail, (GtkImage *) Data->Thumbnail, Width,
		Width / 16 * 9,
		Channel, Mode);

	// Calculate length of tooltip
	int ToolTipLength = 4;

	// Add channel
	if (Row->Creator != NULL) {
		ToolTipLength += strlen(Row->Creator + 7);
	}

	// Add publication
	if (Row->Title != NULL) {
		ToolTipLength += strlen(Row->Title);
	}

	if (ToolTipLength != 4) {
		// Create tooltip
		char ToolTip[ToolTipLength];

		// Write either "channel - publication", "channel" or "publication"
		if (Row->Creator != NULL && Row->Title != NULL) {
			sprintf(ToolTip, "%s - %s", Row->Creator + 7, Row->Title);
		} else if (Row->Creator != NULL) {
			sprintf(ToolTip, "%s", Row->Creator + 7);
		} else {
			sprintf(ToolTip, "%s", Row->Title);
		}

		// Set tooltip
		gtk_widget_set_tooltip_text(Data->Box, ToolTip);
	}

	// Create and add either row or column to box
	GtkOrientation Orientation =
		(Grid) ? GTK_ORIENTATION_VERTICAL : GTK_ORIENTATION_HORIZONTAL;
	GtkWidget *RowColumn = gtk_box_new(Orientation, 0);
	gtk_container_add((GtkContainer *) Data->Box, RowColumn);

	// Get Holder and its children
	GObject *Holder = gtk_builder_get_object(GlobalBuilder, "Holder");
	GList *Children = gtk_container_get_children((GtkContainer *) Holder);

	// Reparent children, free list, destroy Holder
	g_list_foreach(Children, (GFunc) BoxReparent, RowColumn);
	g_list_free(Children);
	gtk_widget_destroy((GtkWidget *) Holder);

	// Set paddings
	gtk_widget_set_margin_start(RowColumn, Padding);
	gtk_widget_set_margin_end(RowColumn, Padding);
	gtk_widget_set_margin_top(RowColumn, Padding);
	gtk_widget_set_margin_bottom(RowColumn, Padding);

	// Get Confirmation and its label
	GObject *Confirmations = gtk_builder_get_object(GlobalBuilder,
			"Confirmations");
	GObject *ConfirmationsLabel = gtk_builder_get_object(GlobalBuilder,
			"ConfirmationsLabel");
	GObject *Action = gtk_builder_get_object(GlobalBuilder, "Action");

	// Set data depending on type

	// Set File type as regular
	// TODO: Change this when file actions are implemented
	if (Type == 3) {
		Type = 0;
	}

	// Not wallet, hide confirmation, else set it
	if (Type != 2) {
		gtk_widget_set_no_show_all((GtkWidget *) Confirmations, true);
		gtk_widget_set_no_show_all((GtkWidget *) ConfirmationsLabel, true);
	} else {
		char Label[snprintf(NULL, 0, "%ld", Row->Confirmations) + 1];
		sprintf(Label, "%ld", Row->Confirmations);
		GTKExtraNumberLabel((GtkWidget *) Confirmations, Row->Confirmations);
		gtk_widget_set_tooltip_text((GtkWidget *) Confirmations, Label);
	}

	// Not regular publication, set action, else hide it
	if (Type != 0) {
		gtk_label_set_label((GtkLabel *) Action, Row->Action);
	} else {
		gtk_widget_set_no_show_all((GtkWidget *) Action, true);
	}

	// Set type
	gtk_label_set_label((GtkLabel *) Data->Type, Row->Type);

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect, Data);

	// Add to FlowBox
	gtk_container_add((GtkContainer *) FlowBox, Data->Box);

	// Get middle child and its style
	GtkWidget *FlowChild = gtk_widget_get_parent(Data->Box);
	GtkStyleContext *StyleContext = gtk_widget_get_style_context(FlowChild);

	// Set padding free style, align to center in grid mode
	gtk_style_context_add_provider(StyleContext,
		(GtkStyleProvider *) GTKExtraCss,
		GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	if (Grid) {
		gtk_widget_set_halign(FlowChild, GTK_ALIGN_CENTER);
	}

	return Data;
}

gboolean BoxOnTitleBoxDraw(GtkWidget *Widget, cairo_t *Cairo, BoxData *Data) {
	// This function is responsible for (re)creating the title column, if needed

	// If there is no title, exit
	if (Data->Title == NULL) {
		return FALSE;
	}

	// Get width, if not grid, otherwise use preset
	int Width = Data->TextWidth;
	if (!Data->Grid) {
		Width = gtk_widget_get_allocated_width(Widget);
	}

	// If row width did not change, exit
	if ((Width == 0 || Data->TextWidth == Width) && !Data->Grid) {
		return FALSE;
	}

	// Remove previously displayed rows
	gtk_container_foreach((GtkContainer *) Widget,
		(GtkCallback) GTKExtraDestroy, NULL);

	// Create column keeping title
	ColumnCreate(Data->Title, Widget, Width, Data->TitleRows, true, true,
		false, false);

	// Display widget and save new width
	gtk_widget_show_all(Widget);
	Data->TextWidth = Width;

	// Grid column is only made once
	Data->Grid = false;

	return FALSE;
}

gboolean BoxOnButtonPressEvent(GtkWidget *Widget, GdkEventButton *Event,
	BoxData *Data) {
	// This function is responsible for opening content in same/new page on
	// left/middle click

	// If other buttons are pressed or box does not contain publication, exit
	if ((Event->button != GDK_BUTTON_PRIMARY &&
		Event->button != GDK_BUTTON_MIDDLE) || Data->Url == NULL) {
		return FALSE;
	}

	if (Event->button == GDK_BUTTON_PRIMARY) {
		// If left button is pressed
		// Get GIL state
		PyGILState_STATE State = PyGILState_Ensure();

		// Create arguments, call function, free arguments, release GIL
		PyObject *Arguments = Py_BuildValue("(s)", Data->Url);
		Py_DECREF(PyObject_Call(Data->GetPublicationThread, Arguments, NULL));
		Py_DECREF(Arguments);
		PyGILState_Release(State);
	} else if (Event->button == GDK_BUTTON_MIDDLE) {
		// If middle button is pressed
		// Get GIL state
		PyGILState_STATE State = PyGILState_Ensure();

		// Create base arguments
		PyObject *BaseArguments = PyList_New(1);
		PyList_SetItem(BaseArguments, 0, PyUnicode_FromString(Data->Url));

		// Create list for function and data, fill it
		PyObject *FinalList = PyList_New(2);
		PyList_SetItem(FinalList, 0, PyUnicode_FromString("Publication"));
		PyList_SetItem(FinalList, 1, BaseArguments);

		// Create arguments with dummy GTK values, call function
		PyObject *Arguments = Py_BuildValue("ssO", ".", "", FinalList);
		Py_DECREF(FinalList);
		Py_DECREF(PyObject_Call(Data->AddPage, Arguments, NULL));

		// Free arguments, release GIL
		Py_DECREF(Arguments);
		PyGILState_Release(State);
	}

	return FALSE;
}

gboolean BoxOnBoxDestroy(GtkWidget *Widget, BoxData *Data) {
	// This fuction is responsible for freeing every data used by the widget

	// Only free if needed
	if (Data->Url != NULL) {
		free(Data->Url);
	}
	if (Data->Title != NULL) {
		free(Data->Title);
	}

	free(Data);
	return FALSE;
}

// Everything under this line is removable after full C conversion
static PyObject *BoxCreatePython(PyObject *Self, PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get sent data
	long FlowBoxPointer;
	int Grid, Width, TitleRows, Padding, Type, ThumbnailRounding,
		ProfileCircling, ListChannelWidth, PublicationProfile,
		PublicationProfileSize;
	PyObject *GetPublicationThread, *AddPage, *GotRow, *Lister;

	if (!PyArg_ParseTuple(Args, "iiOOiiiOiiOilii", &Grid,
		&Width,
		&GotRow, &GetPublicationThread, &TitleRows, &Padding, &Type, &AddPage,
		&ThumbnailRounding, &ProfileCircling, &Lister,
		&ListChannelWidth, &FlowBoxPointer, &PublicationProfile,
		&PublicationProfileSize)) {
		return NULL;
	}

	if (GetPublicationThread == Py_None) {
		GetPublicationThread = NULL;
	}
	if (AddPage == Py_None) {
		AddPage = NULL;
	}

	BoxPublicationData *Row;

	Row = malloc(sizeof(BoxPublicationData));
	if (Row == NULL) {
		return NULL;
	}

	PyObject *TypeObject = PyDict_GetItemString(GotRow, "Type");
	PyObject *ThumbnailObject = PyDict_GetItemString(GotRow, "Thumbnail");
	PyObject *TitleObject = PyDict_GetItemString(GotRow, "Title");
	PyObject *UrlObject = PyDict_GetItemString(GotRow, "Url");
	PyObject *TimeStampObject = PyDict_GetItemString(GotRow, "TimeStamp");
	PyObject *NumberObject = PyDict_GetItemString(GotRow, "Number");
	PyObject *ValueObject = PyDict_GetItemString(GotRow, "Value");
	PyObject *SupportObject = PyDict_GetItemString(GotRow, "Support");
	PyObject *ProfileObject = PyDict_GetItemString(GotRow, "Profile");
	PyObject *CreatorObject = PyDict_GetItemString(GotRow, "Creator");
	PyObject *NameObject = PyDict_GetItemString(GotRow, "Name");
	PyObject *ActionObject = PyDict_GetItemString(GotRow, "Action");
	PyObject *ConfirmationsObject =
		PyDict_GetItemString(GotRow, "Confirmations");

	Row->TimeStamp = PyLong_AsLong(TimeStampObject);
	Row->Number = PyLong_AsLong(NumberObject);
	Row->Value = PyFloat_AsDouble(ValueObject);
	Row->Support = PyFloat_AsDouble(SupportObject);
	Row->Confirmations = PyLong_AsLong(ConfirmationsObject);
	Row->Type = (char *) PyUnicode_AsUTF8(TypeObject);
	Row->Thumbnail = (char *) PyUnicode_AsUTF8(ThumbnailObject);
	Row->Title = (char *) PyUnicode_AsUTF8(TitleObject);
	Row->Url = (char *) PyUnicode_AsUTF8(UrlObject);
	Row->Profile = (char *) PyUnicode_AsUTF8(ProfileObject);
	Row->Creator = (char *) PyUnicode_AsUTF8(CreatorObject);
	Row->Name = (char *) PyUnicode_AsUTF8(NameObject);
	Row->Action = (char *) PyUnicode_AsUTF8(ActionObject);

	if (strlen(Row->Type) == 0) {
		Row->Type = NULL;
	}
	if (strlen(Row->Thumbnail) == 0) {
		Row->Thumbnail = NULL;
	}
	if (strlen(Row->Title) == 0) {
		Row->Title = NULL;
	}
	if (strlen(Row->Url) == 0) {
		Row->Url = NULL;
	}
	if (strlen(Row->Profile) == 0) {
		Row->Profile = NULL;
	}
	if (strlen(Row->Creator) == 0) {
		Row->Creator = NULL;
	}
	if (strlen(Row->Action) == 0) {
		Row->Action = NULL;
	}
	if (strlen(Row->Name) == 0) {
		Row->Name = NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	BoxData *Data = BoxCreate((bool) Grid, Width, Row, GetPublicationThread,
			TitleRows, Padding, Type, AddPage,
			(bool) ThumbnailRounding, (bool) ProfileCircling, Lister,
			ListChannelWidth, (GtkWidget *) FlowBoxPointer,
			(bool) PublicationProfile, PublicationProfileSize);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->Box);
	PyDict_SetItemString(Dict, "Box", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Type);
	PyDict_SetItemString(Dict, "Type", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

// Function names and other python data
static PyMethodDef BoxMethods[] = {
	{"Create", BoxCreatePython, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef BoxModule = {
	PyModuleDef_HEAD_INIT, "Box", "Box module", -1, BoxMethods
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Box(void) {
	return PyModule_Create(&BoxModule);
}
