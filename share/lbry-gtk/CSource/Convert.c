/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for converting a file into GTK compatible
// type and returning if it was able to do it, in a different process

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <MagickWand/MagickWand.h>

int main(int Count, char **Arguments) {
	// This function is responsible for converting a file into GTK compatible
	// type and returning if it was able to do it

	// Proper call checking
	if (Count < 3) {
		printf("0");
		return 0;
	}

	// Start ImageMagick
	MagickWandGenesis();

	// Create wand, exit if unsuccessful
	MagickWand *Wand = NewMagickWand();
	if (Wand == NULL) {
		printf("0");
		MagickWandTerminus();
		return 0;
	}

	// Get file and supported types
	char *FilePath = Arguments[1];
	char *Types[Count - 2];
	for (int Index = 0; Index < Count - 2; ++Index) {
		Types[Index] = Arguments[Index + 2];
	}

	// Add .gif to name
	char LocalPath[strlen(FilePath) + 5];
	sprintf(LocalPath, "%s.gif", FilePath);

	// Check if gif version exists
	MagickBooleanType Status = MagickPingImage(Wand, LocalPath);

	// If not, load regular version
	if (Status != MagickTrue) {
		Status = MagickPingImage(Wand, FilePath);
	}

	if (Status == MagickTrue) {
		// If it was able to load an image

		// Get format, convert to lowercase
		char *Format = MagickGetImageFormat(Wand);
		for (int Index = 0; Index < (int) strlen(Format); ++Index) {
			Format[Index] = tolower(Format[Index]);
		}

		// Go through every format signify and exit if file format is good
		bool Good = false;
		for (int Index = 0; Index < Count - 2; ++Index) {
			if (strcmp(Types[Index], Format) == 0) {
				Good = true;
				break;
			}
		}

		free(Format);
		if (!Good) {
			//If GTK can not handle the format

			// Empty wand, load full image
			ClearMagickWand(Wand);
			Status = MagickReadImage(Wand, FilePath);
			if (Status == MagickTrue) {
				// If image is a single frame
				if (MagickGetNumberImages(Wand) == 1) {
					// Save as png
					MagickSetFormat(Wand, "PNG");
					MagickWriteImage(Wand, FilePath);
				} else {
					// Save as gif under new name
					MagickSetFormat(Wand, "GIF");
					MagickWriteImages(Wand, LocalPath, true);
				}
			}
		}
	}

	// Destroy wand, stop ImageMagick, return success or failure
	DestroyMagickWand(Wand);
	MagickWandTerminus();
	printf("%d", Status == MagickTrue);
	return 0;
}
