/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This is the main executable file

#include <Python.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

#include "Header/OS.h"

const char *Imports[] = {"gi", "json", "sys", "threading", "datetime", "time",
	"os", "queue", "re", "copy", "traceback", "shutil", "pathlib", NULL };

int main(int argc, char *argv[]) {
	// Initialize python interpreter
	Py_Initialize();

	// This is rather odd, one might say, but this is necessary 
	// as main thread (C main) takes the ownership of GIL
	// and doesn't release it once other threads are running
	// evidently killing threading.
	PyThreadState *Save = PyEval_SaveThread();
	PyGILState_STATE State = PyGILState_Ensure();

	// All the modules tested in an array
	PyObject *Module;
	for (int Index = 0; Imports[Index] != NULL; Index++) {
		Module = PyImport_ImportModule(Imports[Index]);
		if (Module == NULL) {
			return 1;
		}
		Py_DECREF(Module);
	}

	// Get root directory
	char *Root = OSAbsolutePath(argv[0]);
	int Indexes[] = { 0, 0 };
	for (int Index = 0; Root[Index] != '\0'; Index++) {
		if (Root[Index] == OSSeparator) {
			Indexes[0] = Indexes[1];
			Indexes[1] = Index;
		}
	}

	Root[Indexes[0]] = '\0';
	char ModuleDir[strlen(Root) + 30];

	// Append to module path
	PyObject *Path = PySys_GetObject("path");
	sprintf(ModuleDir, "%s%cshare%clbry-gtk", Root, OSSeparator, OSSeparator);
	PyObject *Item = PyUnicode_FromString(ModuleDir);
	PyList_Append(Path, Item);
	Py_DECREF(Item);
	free(Root);

	sprintf(ModuleDir + strlen(ModuleDir), "%cCSource", OSSeparator);
	Item = PyUnicode_FromString(ModuleDir);
	PyList_Append(Path, Item);
	Py_DECREF(Item);

	// Import main module
	PyObject *Name = PyUnicode_FromString("Source.Main");
	PyObject *MainModule = PyImport_Import(Name);
	Py_DECREF(Name);
	if (MainModule == NULL) {
		PyErr_Print();
		PyGILState_Release(State);
		Py_Finalize();
		return 1;
	}

	// Get Main object and execute it with path argument
	PyObject *Func = PyObject_GetAttrString(MainModule, "Main");
	PyObject *Args = Py_BuildValue("(s)", argv[0]);
	PyObject *Call = PyObject_CallObject(Func, Args);
	Py_DECREF(MainModule);
	Py_DECREF(Func);
	Py_DECREF(Args);
	Py_DECREF(Call);

	// Release GIL from C main
	PyGILState_Release(State);

	// Gtk main started here
	gtk_main();

	// Finalize Python and get out
	PyEval_RestoreThread(Save);
	Py_Finalize();

	return 0;
}
