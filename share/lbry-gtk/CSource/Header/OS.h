#ifndef OS_H_
#define OS_H_

// Type of OS
extern int OSType;

// Default opener application
extern char *OSOpener;

// Directory separator
extern char OSSeparator;

// This function is responsible for getting the absolute path to binary
// For uniformity
char *OSAbsolutePath(char *Arg0);

#endif
