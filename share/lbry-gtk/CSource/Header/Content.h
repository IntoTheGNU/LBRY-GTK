#ifndef CONTENT_H_
#define CONTENT_H_

#include <jansson.h>

// This function searches for content on LBRY
json_t *ContentSearch(json_t *Params, int PageSize, int Page, char *Server);

#endif
