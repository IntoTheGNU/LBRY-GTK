#ifndef WALLET_H_
#define WALLET_H_

// This gets the transaction list
json_t *WalletHistory(int PageSize, int Page, char *Server);

// This gets the wallet balance
json_t *WalletBalance(char *Server);

#endif
