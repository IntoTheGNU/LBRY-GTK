#ifndef REPLACE_H_
#define REPLACE_H_

#include <gtk/gtk.h>

// Max function
#define Max(i, j) (((i) > (j)) ? (i) : (j))

// Struct with every data needed for operation
typedef struct ReplaceData {
	char *Current;
	char **Names;
	GtkWidget **Widgets, *Space;
	int Length;
} ReplaceData;

// This function is responsible for creating main data structure containing
// everything needed by functions
ReplaceData *ReplaceInit(GtkWidget **GotWidgets, char **GotNames,
	GtkWidget *GotSpace, int GotLength);

// This function is responsible for replacing current widget with one called
// by Name
void ReplaceSet(ReplaceData *Data, char *Name);

// This function is responsible for getting the name of the current widget
char *ReplaceGet(ReplaceData *Data);

#endif
