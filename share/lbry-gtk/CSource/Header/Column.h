#ifndef COLUMN_H_
#define COLUMN_H_

// This function is responsible for creating a set sized Column of set
// number of lines
void ColumnCreate(char *GotText, GtkWidget *Box, int Width, int Lines,
	bool Dots, bool Dashes, bool Force, bool Middle);

#endif
