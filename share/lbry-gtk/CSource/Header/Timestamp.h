#ifndef TIMESTAMP_H_
#define TIMESTAMP_H_

// This function is used to return how long ago something happened
// Remember to free() once done with variable
char *TimestampPassed(unsigned int Timestamp);

// This function returns a date usually used as a tooltip
// Remember to free() once done with variable
char *TimestampGet(unsigned int Timestamp);

// This function returns the days in a month
int TimestampMonthDays(int Year, int Month);

#endif
