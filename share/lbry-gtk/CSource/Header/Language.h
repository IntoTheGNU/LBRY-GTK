#ifndef LANGUAGE_H_
#define LANGUAGE_H_

#include <gtk/gtk.h>

#include "Tag.h"

// Liststore for completing languages
extern GtkListStore *LanguageModel;

// Struct for widget data
typedef struct LanguageData {
	// Subwidgets of Language used in and outside of this file
	GtkWidget *Language, *Combo, *FlowBox;

	// Underlying tagger managing the languages
	TagData *Tagger;
} LanguageData;

// This function is responsible for creating the Language widget
LanguageData *LanguageCreate();

// This function is responsible for clicking add on keybind left clicks and
// opening the combobox on keybind middle clicks
void LanguageKeybindHelper(LanguageData *Data, int Button);

#endif
