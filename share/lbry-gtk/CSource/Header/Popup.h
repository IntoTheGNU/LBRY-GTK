#ifndef POPUP_H_
#define POPUP_H_

// For getting DeveloperMode
extern char *PopupDeveloper[];

// This function is responsible for starting PopupHelper in the main thread
void PopupMessage(char *Text);

// This function is responsible for checking if the user enabled
// DeveloperMode and forwarding data to PopupMessage, if so
void PopupError(char *Text);

#endif
