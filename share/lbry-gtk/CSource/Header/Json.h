#ifndef JSON_H_
#define JSON_H_

#include <Python.h>
#include <stdbool.h>
#include <jansson.h>

// This function is responsible for fetching an object from a json, by keys
json_t *JsonObject(json_t *Json, char **Keys);

// This function is responsible for returning string value of json object
// if it is a string, FallBack otherwise
char *JsonString(json_t *Json, char *FallBack);

// This function is responsible for getting json object from Json and Keys
// then getting string value of it
char *JsonStringObject(json_t *Json, char **Keys, char *FallBack);

// This function is responsible for getting json object from Json and Keys
// then getting bool value of it
bool JsonBoolObject(json_t *Json, char **Keys, bool FallBack);

// This function is responsible for getting json object from Json and Keys
// then getting int value of it
int JsonIntObject(json_t *Json, char **Keys, int FallBack);

// This function is responsible for getting json object from Json and Keys
// then getting double value of it
double JsonDoubleObject(json_t *Json, char **Keys, double FallBack);

// This function is responsible for cleaning unused json values
void JsonClear(json_t *Json);

// This function is responsible for converting c json object to python string
PyObject *JsonToString(json_t *Json);

#endif
