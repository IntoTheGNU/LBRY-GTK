#ifndef URL_H_
#define URL_H_

#include <jansson.h>

// Data for getting json paths
extern char *UrlThumbnailPath1[], *UrlThumbnailPath2[], *UrlTypePath1[];
extern char *UrlTypePath2[], *UrlRepostPath1[], *UrlRepostPath2[];

// List of web instances of LBRY
extern char *UrlInstances[6][3];

// Number of items in Instances
extern int UrlSize;

// Struct for a single instance
struct UrlInstance {
	char *Provider;
	char *Url;
};

// This function is responsible for getting web instance links
struct UrlInstance **UrlWeb(char *GotUrl);

// This function is responsible for getting data about (a) publication(s)
json_t *UrlGet(char **Querries, int QuerryNumber, char *Server);

// This function is responsible for getting thumbnails of reposted claims
// from original publication
void UrlThumbnail(json_t *Data, char *Server);

#endif
