#ifndef OPEN_H_
#define OPEN_H_

// Max function
#define Max(i, j) (((i) > (j)) ? (i) : (j))

// This function is responsible for creating a full command from a Uri and
// an Application, if any, then running it with GLib
void OpenFile(char *Uri, char *Application);

#endif
