#ifndef TAG_H_
#define TAG_H_

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>

// Struct for widget data
typedef struct TagData {
	// Subwidgets of Tag used in and outside of this file
	GtkWidget *FlowBox, *Entry, *Tag, *LanguageEntry;

	// Whether the tags are editable (remove/add)
	bool Edit;

	// Temporary mousekey holder
	int MouseKey;

	// Actual tags
	char **Tags;

	// Function for new page, function holder for current page
	// TODO: do this with C functions
	PyObject *AddPage, *Lister;
} TagData;

// This function is responsible for creating the Tag widget
TagData *TagCreate(bool Edit, PyObject *AddPage);

// This function is responsible for clicking Add, when return is pressed
gboolean TagOnEntryKeyPressEvent(GtkWidget *Widget, GdkEventKey *Event,
	TagData *Data);

// This function is responsible for adding an array of tags
void TagAppend(char **List, TagData *Data);

// This function is responsible for adding tag, when add is clicked
gboolean TagOnAddClicked(GtkWidget *Widget, TagData *Data);

// This function is responsible for removing a single tag, if it is selected
void TagRemove(gpointer Widget, TagData *Data);

#endif
