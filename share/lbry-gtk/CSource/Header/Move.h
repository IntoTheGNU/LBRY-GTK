#ifndef MOVE_H_
#define MOVE_H_

#include <gtk/gtk.h>

// This function is responsible for moving left or right in a flowbox
void MoveFlowBoxLeftRight(GtkFlowBox *FlowBox, int Step);

// This function is responsible for checking if widget is in top line in a
// flowbox and increasing Columns, if so
void MoveAllocationTest(gpointer Widget, gpointer Columns);

// This function is responsible for moving up or down in a flowbox
void MoveFlowBoxUpDown(GtkFlowBox *FlowBox, int Direction);

// This function is responsible for moving up or down in a listview
void MoveListViewUpDown(GtkTreeView *TreeView, int Direction);

#endif
