/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file bundles all modules in LBRY-GTK CRewrite into one supermodule
// This entire file is removeable after full C rewrite

#include <Python.h>

#define ADD_MODULE(P, M) \
	PyObject *M = PyInit_ ## M(); \
	Py_INCREF(M); \
	PyModule_AddObject(P, #M, M)

// All of the module initiation functions
extern PyMODINIT_FUNC PyInit_Box(void);
extern PyMODINIT_FUNC PyInit_Channel(void);
extern PyMODINIT_FUNC PyInit_Collection(void);
extern PyMODINIT_FUNC PyInit_Comments(void);
extern PyMODINIT_FUNC PyInit_Connect(void);
extern PyMODINIT_FUNC PyInit_Content(void);
extern PyMODINIT_FUNC PyInit_DateTime(void);
extern PyMODINIT_FUNC PyInit_File(void);
extern PyMODINIT_FUNC PyInit_Fill(void);
extern PyMODINIT_FUNC PyInit_Following(void);
extern PyMODINIT_FUNC PyInit_Global(void);
extern PyMODINIT_FUNC PyInit_GTKExtra(void);
extern PyMODINIT_FUNC PyInit_Icons(void);
extern PyMODINIT_FUNC PyInit_Image(void);
extern PyMODINIT_FUNC PyInit_KeyBind(void);
extern PyMODINIT_FUNC PyInit_Language(void);
extern PyMODINIT_FUNC PyInit_MDParse(void);
extern PyMODINIT_FUNC PyInit_Move(void);
extern PyMODINIT_FUNC PyInit_Order(void);
extern PyMODINIT_FUNC PyInit_Open(void);
extern PyMODINIT_FUNC PyInit_Places(void);
extern PyMODINIT_FUNC PyInit_Popup(void);
extern PyMODINIT_FUNC PyInit_Preferences(void);
extern PyMODINIT_FUNC PyInit_Profile(void);
extern PyMODINIT_FUNC PyInit_Replace(void);
extern PyMODINIT_FUNC PyInit_SidePanel(void);
extern PyMODINIT_FUNC PyInit_Status(void);
extern PyMODINIT_FUNC PyInit_Support(void);
extern PyMODINIT_FUNC PyInit_Sync(void);
extern PyMODINIT_FUNC PyInit_Tag(void);
extern PyMODINIT_FUNC PyInit_Thumbnail(void);
extern PyMODINIT_FUNC PyInit_Timestamp(void);
extern PyMODINIT_FUNC PyInit_Tray(void);
extern PyMODINIT_FUNC PyInit_Url(void);
extern PyMODINIT_FUNC PyInit_Wallet(void);

extern PyMODINIT_FUNC PyInit_CommentDB(void);
extern PyMODINIT_FUNC PyInit_ConnectDB(void);
extern PyMODINIT_FUNC PyInit_NotificationDB(void);

static struct PyModuleDef CRewriteModule = {
	PyModuleDef_HEAD_INIT, "CRewrite", "CRewrite module", -1, NULL
};

// The master of all inits
PyMODINIT_FUNC PyInit_CRewrite(void) {
	char *Credits = "This CRewrite is written by:\n"
		"\tMorsMortium <morsmortium@gmail.com>\n"
		"\tErwinjitsu <erwinjitsu@proton.me>\n"
		"\tIntoTheGNU <intothegnu@protonmail.com>";

	// Main module
	PyObject *MainModule = PyModule_Create(&CRewriteModule);
	PyModule_AddObject(MainModule, "Credits", PyUnicode_FromString(Credits));

	// All the other modules
	ADD_MODULE(MainModule, Box);
	ADD_MODULE(MainModule, Channel);
	ADD_MODULE(MainModule, Collection);
	ADD_MODULE(MainModule, Comments);
	ADD_MODULE(MainModule, Connect);
	ADD_MODULE(MainModule, Content);
	ADD_MODULE(MainModule, DateTime);
	ADD_MODULE(MainModule, File);
	ADD_MODULE(MainModule, Fill);
	ADD_MODULE(MainModule, Following);
	ADD_MODULE(MainModule, GTKExtra);
	ADD_MODULE(MainModule, Global);
	ADD_MODULE(MainModule, Icons);
	ADD_MODULE(MainModule, Image);
	ADD_MODULE(MainModule, KeyBind);
	ADD_MODULE(MainModule, Language);
	ADD_MODULE(MainModule, MDParse);
	ADD_MODULE(MainModule, Move);
	ADD_MODULE(MainModule, Open);
	ADD_MODULE(MainModule, Order);
	ADD_MODULE(MainModule, Places);
	ADD_MODULE(MainModule, Popup);
	ADD_MODULE(MainModule, Preferences);
	ADD_MODULE(MainModule, Profile);
	ADD_MODULE(MainModule, Replace);
	ADD_MODULE(MainModule, SidePanel);
	ADD_MODULE(MainModule, Status);
	ADD_MODULE(MainModule, Support);
	ADD_MODULE(MainModule, Sync);
	ADD_MODULE(MainModule, Tag);
	ADD_MODULE(MainModule, Thumbnail);
	ADD_MODULE(MainModule, Timestamp);
	ADD_MODULE(MainModule, Tray);
	ADD_MODULE(MainModule, Url);
	ADD_MODULE(MainModule, Wallet);

	ADD_MODULE(MainModule, CommentDB);
	ADD_MODULE(MainModule, ConnectDB);
	ADD_MODULE(MainModule, NotificationDB);

	// Return our big mega module
	return MainModule;
}
