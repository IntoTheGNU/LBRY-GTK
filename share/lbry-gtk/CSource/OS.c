/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for operating system specific parts

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Header/OS.h"

#ifdef _WIN32

// Type of OS
int OSType = 0;

// Default opener application
char *OSOpener = "start";

// Directory separator
char OSSeparator = '\\';
#else
char OSSeparator = '/';
#endif

#ifdef __APPLE__
int OSType = 1;
char *OSOpener = "open";
#endif

#ifdef __unix__
int OSType = 2;
char *OSOpener = "xdg-open";
#endif

char *OSAbsolutePath(char *Arg0) {
	// This function is responsible for getting the absolute path to binary
	// For uniformity
	char *Absolute = malloc(2);
	sprintf(Absolute, " ");

	if (OSType == 0) {
		// Windows arg 0 contains absolute path, make it malloced for uniformity
		Absolute = realloc(Absolute, strlen(Arg0) + 1);
		sprintf(Absolute, "%s", Arg0);
	}

	if (OSType == 2) {
		// Get proc file for executable
		FILE *File = fopen("/proc/self/maps", "r");
		char Character;

		// Get length of first line in proc file
		int Length = 1;
		while ((Character = fgetc(File)) != '\n') {
			++Length;
		}

		// Get first line, close file
		char Line[Length];
		rewind(File);
		char *Read = fgets(Line, Length, File);
		if (Read == NULL) {
			return NULL;
		}
		fclose(File);

		// Remove everything before last space (including space)
		char *Space = strrchr(Line, ' ');
		memmove(Line, Space + 1, strlen(Space));

		// Make path malloced
		Absolute = realloc(Absolute, strlen(Line) + 1);
		sprintf(Absolute, "%s", Line);
	}
	return Absolute;
}
