################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import json, math

from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import Popup, Preferences, Wallet

CAPI = PyGObjectCAPI()


def BalanceHelper(Function, WalletSpaceParts, **kwargs):
    LBRYSettings = json.loads(Preferences.Get())
    Session = LBRYSettings["Preferences"]["Session"]
    GotBalance = Wallet.Balance(Session["Server"])
    GotBalance = json.loads(GotBalance)
    if "error" in GotBalance:
        Popup.Error(GotBalance["error"])
        Data = Function(**kwargs)
        Data = json.loads(Data)
        return Data
    Values = [
        GotBalance["total"],
        GotBalance["available"],
        GotBalance["reserved"],
        GotBalance["reserved_subtotals"]["claims"],
        GotBalance["reserved_subtotals"]["supports"],
        GotBalance["reserved_subtotals"]["tips"],
    ]
    for i in range(6):
        Value = float(Values[i])
        WalletSpaceParts[i].set_label(str(Value))
    return json.loads(Function(**kwargs))


def InboxHelper(Function, **kwargs):
    Data = Function("", kwargs["page"], kwargs["page_size"], kwargs["server"])
    return json.loads(Data)


def SearchHelper(Function, **kwargs):
    Page, PageSize = kwargs["page"], kwargs["page_size"]
    Server = kwargs["server"]
    del kwargs["page"], kwargs["page_size"], kwargs["server"]
    return json.loads(Function(json.dumps(kwargs), PageSize, Page, Server))


def FileHelper(Function, **kwargs):
    Data = Function(kwargs["page_size"], kwargs["page"], kwargs["server"])
    return json.loads(Data)


def ListContent(MainSpace, Box):
    BoxHeight = Box.get_preferred_height().minimum_height
    if BoxHeight == 0:
        return 1
    return MainSpace.get_allocated_height() // BoxHeight


def GridContent(MainSpace, Box):
    BoxHeight = Box.get_preferred_height().minimum_height
    BoxWidth = Box.get_preferred_width().minimum_width
    if BoxWidth == 0 or BoxHeight == 0:
        return 1
    HorizontalContent = MainSpace.get_allocated_width() // BoxWidth
    VerticalContent = MainSpace.get_allocated_height() // BoxHeight
    return HorizontalContent * VerticalContent
